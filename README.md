INTRODUCTION
------------

Drupal added the html5shiv asset library in 2011 as a polyfill for browsers that
did not support HTML5.

In 2019 the browser support policy was updated, there are no longer any browsers
that require this polyfill.

The core asset library core/html5shiv is deprecated in Drupal 8.8 and removed in
Drupal 9.

See: https://www.drupal.org/node/3086383

This module provides the same asset library for any sites which continue to need
support for older browsers.

INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module.

 * In your theme or module attach the html5shiv/html5shiv asset library. Visit 
   https://www.drupal.org/docs/8/api/javascript-api/add-javascript-to-your-theme-or-module
   for further information.

MAINTAINERS
-----------

Current maintainers:
 * Peter Weber (zrpnr) - https://www.drupal.org/u/zrpnr

